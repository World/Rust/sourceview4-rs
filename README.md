# sourceview4-rs

**Note**: sourceview4-rs is no longer actively maintained as well as other gtk3-rs crates.

Rust bindings for GtkSourceView 4

Documentations: https://world.pages.gitlab.gnome.org/Rust/sourceview4-rs
